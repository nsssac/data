## Student Engagement Conversation between Webmaster and Vice President

Webmaster: `You`
Vice President: `Aleksi`

```
Aleksi sent Yesterday at 22:10
I don't think I've told you anything about our survey idea, have I?

Aleksi sent Yesterday at 22:10
Take a look at this when you get the chance

Aleksi sent Yesterday at 22:11
During the next meeting, TDSB Senators and I are going to be discussing the senators' ideas for questions to include
Aleksi sent Yesterday at 22:11
I think you'd be very good at coming up with ideas, if you could think of something for the meeting I'd greatly appreciate it
13:59
You replied to Aleksi
Today at 13:59. Original message:
I don't think I've told you anything about our survey idea, have I?
Your reply:
No.
You replied to Aleksi
Today at 14:01. Original message:
I think you'd be very good at coming up with ideas, if you could think o…
Your reply:
A few concerns:
You sent Today at 14:03
1. This format may give an advantage to people who are "outgoing" more so than others. For example, discussions in my class mostly centre around 3~5 people out of around 15 people.
You sent Today at 14:04
1. I think this may be mitigated by also conducting an anonymous (cohort and individual) survey online. (not by cohort, but school wide, so respondents cannot be determind by cohort)
You sent Today at 14:07
2. I'm not sure about students attending virtually. Most people attending virtually may find it hard to hear the conversation and engage in it due to latency. (my latency/ping to Rogers @ Montreal (where Google DCs are @) is 11 ms)
You sent Today at 14:08
Also, since meetings don't have many channels and both channels are from essentially the same space, which makes it harder to distinguish people
You sent Today at 14:08
I do like the idea of Cohort rep centred
You sent Today at 14:10
Also, how frequently will this be done?
You sent Today at 14:11
Also (2), will this document be public?
You sent Today at 14:11
I think this kind of transparency increases trust in this body
You sent Today at 14:11
(above)

Aleksi sent Today at 14:11
This is exactly what I was talking about! Insightful comments and ideas
Aleksi sent Today at 14:11
I'm about to do a French quiz, I'll be able to respond after. I do have answers for all of this
You sent Today at 14:13
Thank you

Aleksi replied to you
Today at 14:20. Original message:
1. I think this may be mitigated by also conducting an anonymous (cohort…
Reply by Aleksi:
This is exactly what we plan to do, to accommodate those who don't feel comfortable, or aren't able, to take part in class

Aleksi sent Today at 14:20
That's what #5 in that document is about. Or did you see some disparity between what we wrote and what you envision here?

Aleksi replied to you
Today at 14:21. Original message:
2. I'm not sure about students attending virtually. Most people attendin…
Reply by Aleksi:
We'll be providing the teacher/cohort rep with guidelines for how to lead these discussions, and part of that will cover involving the virtual students
You replied to Aleksi
Today at 14:22. Original message:
This is exactly what we plan to do, to accommodate those who don't feel …
Your reply:
Sorry, didn't read that.
You sent Today at 14:23
Also (3), can this conversation be public? Might be good for transparency, and I don't think it will cause too much harm/no harm.

Aleksi sent Today at 14:23
But with that said, there's a bit of an insurmountable barrier here. Virtual engagement just sucks in class, generally. Have you found anything that teachers can do/have done to accommodate virtual students during their in-person lessons? In my experience, they're pretty much just ignored except for if one of them asks a question, and even then the teacher has to go over to their computer, put on their headphones or whatever, and it's very clunky.

Aleksi replied to you
Today at 14:23. Original message:
I do like the idea of Cohort rep centred
Reply by Aleksi:
That's great, because so do I
You replied to Aleksi
Today at 14:24. Original message:
But with that said, there's a bit of an insurmountable barrier here. Vir…
Your reply:
No. I didn't even know they were doing this virtually in my ENG1D6 class.

Aleksi replied to you
Today at 14:24. Original message:
Also, how frequently will this be done?
Reply by Aleksi:
This is our pilot project. We hope to have cohort reps engaging with their classes on a more regular basis, but for the purposes of supplementing our data about the NSS populace, this is the only iteration of it. This is just our attempt at finding out what works when it comes to class engagement
You replied to Aleksi
Today at 14:25. Original message:
But with that said, there's a bit of an insurmountable barrier here. Vir…
Your reply:
My MPM teacher explicitly asked more questions to virtual students, which I think was good
You sent Today at 14:25
Also, my MPM teacher showed the Google Meet screen on the projector sometimes

Aleksi replied to you
Today at 14:26. Original message:
Also (2), will this document be public?
Reply by Aleksi:
We hadn't planned to make this document public, but like I said we plan to make public the document which explains the guidelines and expectations to the person conducting the discussion. If you think this document should be public, go right ahead! I made it, my permission is all you need.

Aleksi replied to you
Today at 14:26. Original message:
Also (3), can this conversation be public? Might be good for transparenc…
Reply by Aleksi:
Which conversation, and public how?

Aleksi replied to you
Today at 14:26. Original message:
Also, my MPM teacher showed the Google Meet screen on the projector some…
Reply by Aleksi:
Okay great, we can certainly recommend the teacher set that up if they have the technology to accomplish that
You replied to Aleksi
Today at 14:36. Original message:
Which conversation, and public how?
Your reply:
This one, and Ctrl-C+V'd to a .md file
You sent Today at 14:36
(text file)

Aleksi sent Today at 14:37
I'll be honest it's a weird idea
You replied to Aleksi
Today at 14:37. Original message:
I'll be honest it's a weird idea
Your reply:
the public one?

Aleksi sent Today at 14:37
I've never heard of any student council or government group making public their private correspondences between their members
You sent Today at 14:38
I don't think so; court proceedings are for example public, and although these are not too similar, I don't think there are too much downsides

Aleksi sent Today at 14:39
I don't think there are downsides either
Aleksi sent Today at 14:39
Sure, be my guest. I guess you'll start at Sunday 10:10 PM go to today 2:26?
You replied to Aleksi
Today at 14:39. Original message:
Sure, be my guest. I guess you'll start at Sunday 10:10 PM go to today 2…
Your reply:
Yes.

Aleksi sent Today at 14:39
Sounds good.
Aleksi sent Today at 14:39
And if you have ideas for specific survey questions, don't hesitate to send them my way
You sent Today at 14:39
Actually, can we include until 14:40?

Aleksi sent Today at 14:40
Sooner the better, the absolute latest deadline is Thursday noon.
Aleksi sent Today at 14:40
Yeah that works
```
